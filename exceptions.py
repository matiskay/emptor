class EmptorException(Exception):
    pass


class EmptorNetworkException(EmptorException):
    pass
