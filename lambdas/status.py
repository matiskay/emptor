import json
import os
import boto3


def handler(event, context):
    object_id = event['id']
    dynamodb_resource = boto3.resource('dynamodb')
    table = dynamodb_resource.Table(os.environ.get('DYNAMO_DB_TABLE'))

    item = table.get_item(Key={'id': object_id})

    if 'Item' in item:
        return json.dumps(item['Item'])

    return json.dumps({
        'status': 'NOT_EXISTS'
    })
