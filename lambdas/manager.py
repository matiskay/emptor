import json
import os

import boto3

from exceptions import EmptorException
from utils import is_url, generate_id


def manager(url):
    try:
        dynamodb_resource = boto3.resource('dynamodb')
        table = dynamodb_resource.Table(os.environ.get('DYNAMO_DB_TABLE'))
        object_key = generate_id()
        table.put_item(
            Item={
                'id': object_key,
                'url':  url,
                'status': 'PENDING'
            }
        )

        return json.dumps({
            'id': object_key
        })
    except Exception as e:
        raise EmptorException(str(e))


def handler(event, context):
    url = event['url']
    if is_url(url):
        return manager(url)
    raise EmptorException('The url {} is not valid'.format(url))
