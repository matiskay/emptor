import os
import logging
import json
import boto3

import requests
from parsel import Selector

from utils import clean, is_url
from exceptions import EmptorException, EmptorNetworkException


MAX_RETRIES = 3

RETRY_HTTP_CODES = [500, 502, 503, 504, 522, 524, 408]


def crawler(url, object_id):
    try:
        html = fetch(url)

        selector = Selector(text=html)
        title = selector.xpath('//title').extract_first(default='')
        title = clean(title)

        s3_client = boto3.client('s3')
        s3_client.put_object(Bucket=os.environ.get('S3_BUCKET'), Body=html, Key=object_id)
        dynamodb_resource = boto3.resource('dynamodb')
        table = dynamodb_resource.Table(os.environ.get('DYNAMO_DB_TABLE'))
        item = {
            'id': object_id,
            'title': title,
            'status': 'PROCESSED',
            's3_url': 'http://{}.s3.amazonaws.com/{}'.format(os.environ.get('S3_BUCKET'), object_id),
            'url': url
        }
        table.put_item(
            Item=item
        )

        return json.dumps(item)
    except EmptorNetworkException as e:
        dynamodb_resource = boto3.resource('dynamodb')
        table = dynamodb_resource.Table(os.environ.get('DYNAMO_DB_TABLE'))
        table.put_item(
            Item={
                'id': object_id,
                'status': 'STALE',
                'url': url
            }
        )
    except Exception as e:
        raise EmptorException(str(e))


# Request can work with Retry (exponential backoff and those goodies) but YOLO
# https://kite.com/python/docs/requests.urllib3.Retry
def fetch(url):
    count = 0
    while count < MAX_RETRIES:
        count = count + 1
        try:
            r = requests.get(url)

            if r.status_code in RETRY_HTTP_CODES:
                logging.info('{} returns status code {}'.format(url, r.status_code))
                continue

            if r.status_code == 200:
                return r.text

            raise EmptorException('The web page returns {}'.format(r.status_code))
        except Exception as e:
            raise EmptorNetworkException(e)

    raise EmptorNetworkException('The number of retries was exceeded')


def handler(event, context):
    event_data = event['Records'][0]

    if event_data['eventName'] == 'INSERT':
        url = event_data['dynamodb']['NewImage']['url']['S']
        object_id  = event_data['dynamodb']['NewImage']['id']['S']
        if is_url(url):
            return crawler(url, object_id)
        raise EmptorException('The url {} is not valid'.format(url))
