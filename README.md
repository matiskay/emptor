# Emptor Trial Project


### Versions

* [Version 1](https://github.com/matiskay/emptor/tree/5ba0eff4fd3e9051f3e33cc23d75a72ad0fc97b1)
* [Version 2](https://github.com/matiskay/emptor/tree/ac43b642c827f4d3786463935390df50dff98a2a)
* [Version 3](https://github.com/matiskay/emptor/tree/081036b5c1cdaa4f6eb7d98fa0883c9e4c17fe53)
* Version 4

## Installation

1. Install Docker and run it
2. Install npm dependencies `npm install`
3. Create a virtualenv
4. Install requirements-dev.txt using `pip install -r requirements-dev.txt`
5. Run the test `pytest tests/*`

## Usage 

Deployment

```
export AWS_ACCESS_KEY_ID=<your-key-here>
export AWS_SECRET_ACCESS_KEY=<your-secret-key-here>
serverless deploy
```

## Serverless Plugins
* [Python Requirements](https://github.com/UnitedIncome/serverless-python-requirements)
* [Git Variables](https://github.com/jacob-meacham/serverless-plugin-git-variables)

## Notes

* Why I don't create the S3 bucket using serverless resources

In my opinion, S3 bucket should be created in a different cloudformation template because it's hard to delete (the deletion
of the stack may be blocked by S3 bucket with content)

I created it manually for this project

## Architecture

![Fourth Version](./assets/fourth-version.png)
