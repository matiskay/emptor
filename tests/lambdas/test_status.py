import os
import json

import boto3

from moto import mock_dynamodb2

from lambdas.status import handler as status_handler


def test_lambda_status_should_return_item():
    os.environ['DYNAMO_DB_TABLE'] = 'shiny-table'
    os.environ['S3_BUCKET'] = 'shiny-bucket'

    with mock_dynamodb2():
        dynamodb = boto3.resource('dynamodb', 'us-east-1')
        table = dynamodb.create_table(
            TableName=os.environ['DYNAMO_DB_TABLE'],
            KeySchema=[{
                'AttributeName': 'id',
                'KeyType': 'HASH'
            }],
            AttributeDefinitions=[{
                'AttributeName': 'id',
                'AttributeType': 'S'
            }],
            ProvisionedThroughput={
                'ReadCapacityUnits': 1,
                'WriteCapacityUnits': 1
            }
        )

        item = {
            'id': '89dce6a446a69d6b9bdc01ac75251e4c322bcdff',
            'title': 'Awesome Title',
            's3_url': 'http://shiny-bucket.s3.amazonaws.com/89dce6a446a69d6b9bdc01ac75251e4c322bcdff'
        }

        table.put_item(
            TableName=os.environ['DYNAMO_DB_TABLE'],
            Item=item
        )

        assert json.dumps(item) == status_handler({ 'id': '89dce6a446a69d6b9bdc01ac75251e4c322bcdff'}, None)


def test_lambda_status_should_not_return_item():
    os.environ['DYNAMO_DB_TABLE'] = 'shiny-table'
    os.environ['S3_BUCKET'] = 'shiny-bucket'

    with mock_dynamodb2():
        dynamodb = boto3.resource('dynamodb', 'us-east-1')
        table = dynamodb.create_table(
            TableName=os.environ['DYNAMO_DB_TABLE'],
            KeySchema=[{
                'AttributeName': 'id',
                'KeyType': 'HASH'
            }],
            AttributeDefinitions=[{
                'AttributeName': 'id',
                'AttributeType': 'S'
            }],
            ProvisionedThroughput={
                'ReadCapacityUnits': 1,
                'WriteCapacityUnits': 1
            }
        )

        assert json.dumps({'status': 'NOT_EXISTS'}) == status_handler({ 'id': '89dce6a446a69d6b9bdc01ac75251e4c322bcdff'}, None)
