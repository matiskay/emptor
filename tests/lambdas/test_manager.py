import os
import json
import uuid
from unittest.mock import patch

import boto3

from moto import mock_dynamodb2

from lambdas.manager import manager

@patch.object(uuid, 'uuid1', side_effect=['f2919672-80e3-11e9-ae65-5e0094ca3701'])
def test_lambda_manager(_):
    os.environ['S3_BUCKET'] = 'shiny-bucket'
    os.environ['DYNAMO_DB_TABLE'] = 'shiny-table'

    with mock_dynamodb2():
        dynamodb = boto3.resource('dynamodb', 'us-east-1')
        dynamodb.create_table(
            TableName=os.environ['DYNAMO_DB_TABLE'],
            KeySchema=[{
                'AttributeName': 'id',
                'KeyType': 'HASH'
            }],
            AttributeDefinitions=[{
                'AttributeName': 'id',
                'AttributeType': 'S'
            }],
            ProvisionedThroughput={
                'ReadCapacityUnits': 1,
                'WriteCapacityUnits': 1
            }
        )

        expected = {
            'id': 'f2919672-80e3-11e9-ae65-5e0094ca3701'
        }

        assert json.dumps(expected) == manager('http://example.com')




