import os
import json
import uuid
from unittest.mock import patch

import boto3
import requests_mock
from moto import mock_s3, mock_dynamodb2

from lambdas.crawler import crawler, handler as crawler_handler


@patch.object(uuid, 'uuid1', side_effect=['f2919672-80e3-11e9-ae65-5e0094ca3701'])
def test_lambda_crawler(_):
    os.environ['S3_BUCKET'] = 'shiny-bucket'
    os.environ['DYNAMO_DB_TABLE'] = 'shiny-table'

    with mock_s3(), mock_dynamodb2():
        conn = boto3.resource('s3', region_name='us-east-1')
        conn.create_bucket(Bucket=os.environ['S3_BUCKET'])

        dynamodb = boto3.resource('dynamodb', 'us-east-1')
        table = dynamodb.create_table(
            TableName=os.environ['DYNAMO_DB_TABLE'],
            KeySchema=[{
                'AttributeName': 'id',
                'KeyType': 'HASH'
            }],
            AttributeDefinitions=[{
                'AttributeName': 'id',
                'AttributeType': 'S'
            }],
            ProvisionedThroughput={
                'ReadCapacityUnits': 1,
                'WriteCapacityUnits': 1
            }
        )

        with requests_mock.Mocker() as mock:
            url = 'http://example.com'
            mock.get(url, text='<html><title>awesome title</title></html>')

            expected = {
                'id': 'f2919672-80e3-11e9-ae65-5e0094ca3701',
                'title': 'awesome title',
                's3_url': 'http://shiny-bucket.s3.amazonaws.com/f2919672-80e3-11e9-ae65-5e0094ca3701',
                'status': 'PROCESSED',
                'url': 'http://example.com'
            }

            assert expected == json.loads(crawler('http://example.com', 'f2919672-80e3-11e9-ae65-5e0094ca3701'))

            item = table.get_item(Key={'id': 'f2919672-80e3-11e9-ae65-5e0094ca3701'})
            assert item['Item']['id'] == 'f2919672-80e3-11e9-ae65-5e0094ca3701'


@patch.object(uuid, 'uuid1', side_effect=['f2919672-80e3-11e9-ae65-5e0094ca3701'])
def test_lambda_crawler_error(_):
    os.environ['DYNAMO_DB_TABLE'] = 'shiny-table'
    with requests_mock.Mocker() as mock, mock_dynamodb2():
        url = 'http://example.com'
        mock.get(url, text='<html><title>awesome title</title></html>', status_code=400)
        dynamodb = boto3.resource('dynamodb', 'us-east-1')
        table = dynamodb.create_table(
            TableName=os.environ['DYNAMO_DB_TABLE'],
            KeySchema=[{
                'AttributeName': 'id',
                'KeyType': 'HASH'
            }],
            AttributeDefinitions=[{
                'AttributeName': 'id',
                'AttributeType': 'S'
            }],
            ProvisionedThroughput={
                'ReadCapacityUnits': 1,
                'WriteCapacityUnits': 1
            }
        )

        crawler('http://example.com', 'f2919672-80e3-11e9-ae65-5e0094ca3701')

        item = table.get_item(Key={
            'id': 'f2919672-80e3-11e9-ae65-5e0094ca3701'
        })

        assert item['Item']['status'] == 'STALE'

def test_crawler_hanler():
    os.environ['DYNAMO_DB_TABLE'] = 'shiny-table'
    os.environ['S3_BUCKET'] = 'shiny-bucket'

    event = {
        'Records': [
            {
                'eventID': '55b083c521fc2c58457fbb5ac8a301a4',
                'eventName': 'INSERT',
                'eventVersion': '1.1',
                'eventSource': 'aws:dynamodb',
                'awsRegion': 'us-east-1',
                'dynamodb': {
                    'ApproximateCreationDateTime': 1558991250.0,
                    'Keys': {
                        'id': {
                            'S': '1234-5678-91011-12134'
                        }
                    },
                    'NewImage': {
                        'id': {
                            'S': '1234-5678-91011-12134'
                        },
                        'url': {
                            'S': 'https://elcomercio.pe/politica/afrontaron-ultimos-seis-presidentes-desastres-naturales-noticia-639106'
                        },
                        'status': {
                            'S': 'PENDING'
                        }
                    },
                    'SequenceNumber': '11346600000000004314970795',
                    'SizeBytes': 167,
                    'StreamViewType': 'NEW_IMAGE'
                },
                'eventSourceARN': 'arn:aws:dynamodb:us-east-1:769678044694:table/Emptor/stream/2019-05-27T19:41:39.920'
            }
        ]
    }

    with mock_dynamodb2(), mock_s3(), requests_mock.Mocker() as request_mock:
        request_mock.get('https://elcomercio.pe/politica/afrontaron-ultimos-seis-presidentes-desastres-naturales-noticia-639106', text='<html><title>Web Page Title</title></html>')

        conn = boto3.resource('s3', region_name='us-east-1')
        conn.create_bucket(Bucket=os.environ['S3_BUCKET'])

        dynamodb = boto3.resource('dynamodb', 'us-east-1')
        dynamodb.create_table(
            TableName=os.environ['DYNAMO_DB_TABLE'],
            KeySchema=[{
                'AttributeName': 'id',
                'KeyType': 'HASH'
            }],
            AttributeDefinitions=[{
                'AttributeName': 'id',
                'AttributeType': 'S'
            }],
            ProvisionedThroughput={
                'ReadCapacityUnits': 1,
                'WriteCapacityUnits': 1
            }
        )

        result = json.loads(crawler_handler(event, None))

        assert 'PROCESSED' == result['status']
        assert '1234-5678-91011-12134' == result['id']
        assert 'Web Page Title' == result['title']
