from unittest.mock import Mock, patch

import requests_mock
import pytest

from lambdas.crawler import fetch

from utils import make_hash, is_url
from exceptions import EmptorNetworkException



def test_is_url():
    assert is_url('http://example.com') == True
    assert is_url('http://127.0.0.1') == True
    assert is_url('http://0.0.1') == False
    assert is_url('http://6000.0.0.1') == False
    assert is_url('http://256.0.0.1') == False
    assert is_url('abcd') == False


def test_make_hash():
    assert make_hash('wat') == 'a3bbe1a8f2f025b8b6c5b66937763bb2b9bebdf2'


def test_fetch():
    with requests_mock.Mocker() as mock:
        url = 'http://test.com'
        mock.get(url, text='<html><title>Web Page Title</title></html>')

        assert '<html><title>Web Page Title</title></html>' == fetch(url)


def test_fetch_on_400_error():
    with requests_mock.Mocker() as mock:
        url = 'http://test.com'
        mock.get(url, text='<html><title>Web Page Title</title></html>', status_code=400)

        with pytest.raises(EmptorNetworkException):
            fetch(url)


def test_fetch_on_500_error():
    with requests_mock.Mocker() as mock:
        url = 'http://test.com'
        mock.get(url, text='<html><title>Web Page Title</title></html>', status_code=400)

        with pytest.raises(EmptorNetworkException):
            fetch(url)


# requests_mock sucks as dynamic requests
@patch('requests.get')
def test_retries_on_fetch(mock_get):
    response1 = Mock()
    response1.status_code = 500

    response2 = Mock()
    response2.status_code = 500

    response3 = Mock()
    response3.status_code = 200
    response3.text = '<html><title>Web Page Title</title></html>'

    mock_get.side_effect = [response1, response2, response3]

    assert '<html><title>Web Page Title</title></html>' == fetch('http://test.com')


# requests_mock sucks as dynamic requests
@patch('requests.get')
def test_retries_on_fetch_with_fail(mock_get):
    response1 = Mock()
    response1.status_code = 500

    response2 = Mock()
    response2.status_code = 503

    response3 = Mock()
    response3.status_code = 522
    response3.text = '<html><title>Web Page Title</title></html>'

    mock_get.side_effect = [response1, response2, response3]

    with pytest.raises(EmptorNetworkException):
        fetch('http://example.com')




