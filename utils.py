import hashlib
import uuid

from w3lib.html import remove_tags
from validators.url import url as url_validator
from validators.utils import ValidationFailure


def is_url(url):
    result = url_validator(url)
    if isinstance(result, ValidationFailure):
        return False

    return result


def make_hash(value):
    return hashlib.sha1(value.encode('utf8')).hexdigest()


def generate_id():
    return str(uuid.uuid1())


def clean(text):
    text = remove_tags(text)
    text = text.strip()
    return text
